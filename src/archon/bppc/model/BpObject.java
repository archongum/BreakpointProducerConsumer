package archon.bppc.model;


/**
 * 项目中，放到输入和输出队列的通用类型。 <br>
 * 该类型包含两个属性：1，读取输入文件时，记录每一行的行号；2，需要把每一行的字符串转化成自定义类型的值。 <br>
 * 说明：行号row可以理解。下面举例说明{@code T}：如果我想把从输入文件读取到的每一行字符串直接放到输入队列中（{@code BpContext}里的{@code LinkedBlockingQueue<BpObject<IN>>}）,
 * 那么，{@code T}就是{@code String}类型。假设输入文件的每行是人物信息（包含id，name，address等），然后想封装成一个PersonInfo类型（属性有id，name，address等）再放到输入队列，
 * 那么，{@code T}就是PersonInfo类型。
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 * @param <T> 放到输入或输出队列的类型。
 */
public class BpObject<T> {
	
	/**
	 * {@code T} value所在的行号。
	 */
	private int row;
	
	/**
	 * 所在行号的{@code String}对应的类型。
	 */
	private T value;

	public BpObject(int row, T value) {
		this.row = row;
		this.value = value;
	}

	
	/*
	 * Getter, Setter and toString().
	 */
	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "BpObject [row=" + row + ", value=" + value + "]";
	}
}
