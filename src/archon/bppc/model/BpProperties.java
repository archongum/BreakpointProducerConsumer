package archon.bppc.model;

/**
 * 项目所用到的常量。 <br>
 * 可以自定义配置这些常量。 <br>
 * 调用类方法{@link #getDefaultBpProperties()}得到默认配置。
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 */
public class BpProperties {
	
	/**
	 * 默认输入目录路径。
	 */
	private static final String IN_DIR_PATH = "in";
	
	/**
	 * 默认输出目录路径。
	 */
	private static final String OUT_DIR_PATH = "out";
	
	/**
	 * 默认临时目录路径。
	 */
	private static final String TMP_DIR_PATH = "tmp";

	/**
	 * 保存到输出目录的文件的默认前缀。
	 */
	private static final String OUT_FILE_PREFIX = "out_";
	
	/**
	 * 记录断点行号位置的文件的默认前缀。
	 */
	private static final String BP_FILE_PREFIX = "bp_";
	
	/**
	 * 用于解决“第一行”问题的文件的默认前缀。 <br>
	 * 备注：第一行问题：假设第一次运行，处理了2、3、4、5、6、8和9行，1行没有处理；那么记录断点的文件就记录2、3、4、5、6、8和9行； <br>
	 * 下次运行时，由于2到6行是连续的，所以从记录断点的文件中删除2到6行，删除之后为8和9行；这时，就直接跳过了没有处理的1行。
	 */
	private static final String ROW_ONE_FILE_PREFIX = "ro_";
	
	/**
	 * 输入队列的默认容量。
	 */
	private static final int IN_QUEUE_CAPACITY = 1000;
	
	/**
	 * 输出队列的默认容量。
	 */
	private static final int OUT_QUEUE_CAPACITY = 1000;
	
	/**
	 * 输入输出文件的默认字符编码。
	 */
	private static final String CHARSET_NAME = "UTF-8";
	
	/**
	 * 输出队列的消费者，拿输出队列的默认超时。
	 */
	private static final int BP_CONSUMER_POLL_TIMEOUT_MS = 1000;
	
	/**
	 * 输入输出队列之间的消费者，拿输入队列的默认超时。
	 */
	private static final int MID_CONSUMER_POLL_TIMEOUT_MS = 1000;
	
	/**
	 * 输入输出队列之间的消费者，的默认数量，即线程数。
	 */
	private static final int MID_CONSUMER_THREAD_COUNT = 10;
	
	/**
	 * 输入目录路径。
	 */
	private String inDirPath;
	
	/**
	 * 输出目录路径。
	 */
	private String outDirPath;
	
	/**
	 * 临时目录路径。
	 */
	private String tmpDirPath;

	/**
	 * 保存到输出目录的文件的前缀。
	 */
	private String outFilePrefix;
	
	/**
	 * 记录断点行号位置的文件的前缀。
	 */
	private String bpFilePrefix;
	
	/**
	 * 用于解决“第一行”问题的文件的前缀。 <br>
	 * 备注：第一行问题：假设第一次运行，处理了2、3、4、5、6、8和9行，1行没有处理；那么记录断点的文件就记录2、3、4、5、6、8和9行； <br>
	 * 下次运行时，由于2到6行是连续的，所以从记录断点的文件中删除2到6行，删除之后为8和9行；这时，就直接跳过了没有处理的1行。
	 */
	private String rowOneFilePrefix;
	
	/**
	 * 输入队列的容量。
	 */
	private int inQueueCapacity;
	
	/**
	 * 输出队列的容量。
	 */
	private int outQueueCapacity;
	
	/**
	 * 输入输出文件的字符编码。
	 */
	private String charsetName;
	
	/**
	 * 输出队列的消费者，拿输出队列的超时。
	 */
	private int bpConsumerPollTimeoutMs;
	
	/**
	 * 输入输出队列之间的消费者，拿输入队列的超时。
	 */
	private int midConsumerPollTimeoutMs;
	
	/**
	 * 输入输出队列之间的消费者，的数量，即线程数。
	 */
	private int midConsumerThreadCount;

	/**
	 * 获得默认配置。
	 * @return 默认配置。
	 */
	public static BpProperties getDefaultBpProperties() {
		BpProperties bpProperties = new BpProperties();

		bpProperties.setInDirPath(IN_DIR_PATH);
		bpProperties.setOutDirPath(OUT_DIR_PATH);
		bpProperties.setTmpDirPath(TMP_DIR_PATH);

		bpProperties.setOutFilePrefix(OUT_FILE_PREFIX);
		bpProperties.setBpFilePrefix(BP_FILE_PREFIX);
		bpProperties.setRowOneFilePrefix(ROW_ONE_FILE_PREFIX);
		
		bpProperties.setInQueueCapacity(IN_QUEUE_CAPACITY);
		bpProperties.setOutQueueCapacity(OUT_QUEUE_CAPACITY);
		
		bpProperties.setCharsetName(CHARSET_NAME);	
		bpProperties.setBpConsumerPollTimeoutMs(BP_CONSUMER_POLL_TIMEOUT_MS);
		bpProperties.setMidConsumerPollTimeoutMs(MID_CONSUMER_POLL_TIMEOUT_MS);
		bpProperties.setMidConsumerThreadCount(MID_CONSUMER_THREAD_COUNT);
		return bpProperties;
	}

	
	/*
	 * Getter, Setter and toString().
	 */
	public String getInDirPath() {
		return inDirPath;
	}

	public void setInDirPath(String inDirPath) {
		this.inDirPath = inDirPath;
	}

	public String getOutDirPath() {
		return outDirPath;
	}

	public void setOutDirPath(String outDirPath) {
		this.outDirPath = outDirPath;
	}

	public String getTmpDirPath() {
		return tmpDirPath;
	}

	public void setTmpDirPath(String tmpDirPath) {
		this.tmpDirPath = tmpDirPath;
	}

	public String getOutFilePrefix() {
		return outFilePrefix;
	}

	public void setOutFilePrefix(String outFilePrefix) {
		this.outFilePrefix = outFilePrefix;
	}

	public String getBpFilePrefix() {
		return bpFilePrefix;
	}

	public void setBpFilePrefix(String bpFilePrefix) {
		this.bpFilePrefix = bpFilePrefix;
	}

	public String getRowOneFilePrefix() {
		return rowOneFilePrefix;
	}

	public void setRowOneFilePrefix(String rowOneFilePrefix) {
		this.rowOneFilePrefix = rowOneFilePrefix;
	}

	public int getInQueueCapacity() {
		return inQueueCapacity;
	}

	public void setInQueueCapacity(int inQueueCapacity) {
		this.inQueueCapacity = inQueueCapacity;
	}

	public int getOutQueueCapacity() {
		return outQueueCapacity;
	}

	public void setOutQueueCapacity(int outQueueCapacity) {
		this.outQueueCapacity = outQueueCapacity;
	}

	public String getCharsetName() {
		return charsetName;
	}

	public void setCharsetName(String charsetName) {
		this.charsetName = charsetName;
	}

	public int getBpConsumerPollTimeoutMs() {
		return bpConsumerPollTimeoutMs;
	}

	public void setBpConsumerPollTimeoutMs(int bpConsumerPollTimeoutMs) {
		this.bpConsumerPollTimeoutMs = bpConsumerPollTimeoutMs;
	}

	public int getMidConsumerPollTimeoutMs() {
		return midConsumerPollTimeoutMs;
	}

	public void setMidConsumerPollTimeoutMs(int midConsumerPollTimeoutMs) {
		this.midConsumerPollTimeoutMs = midConsumerPollTimeoutMs;
	}

	public int getMidConsumerThreadCount() {
		return midConsumerThreadCount;
	}

	public void setMidConsumerThreadCount(int midConsumerThreadCount) {
		this.midConsumerThreadCount = midConsumerThreadCount;
	}

	@Override
	public String toString() {
		return "BpProperties [inDirPath=" + inDirPath + ", outDirPath=" + outDirPath + ", tmpDirPath=" + tmpDirPath
				+ ", outFilePrefix=" + outFilePrefix + ", bpFilePrefix=" + bpFilePrefix + ", rowOneFilePrefix="
				+ rowOneFilePrefix + ", inQueueCapacity=" + inQueueCapacity + ", outQueueCapacity=" + outQueueCapacity
				+ ", charsetName=" + charsetName + ", bpConsumerPollTimeoutMs=" + bpConsumerPollTimeoutMs
				+ ", midConsumerPollTimeoutMs=" + midConsumerPollTimeoutMs + ", midConsumerThreadCount="
				+ midConsumerThreadCount + "]";
	}
}
