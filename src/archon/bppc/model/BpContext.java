package archon.bppc.model;

import java.io.File;
import java.io.IOException;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 上下文类。 <br>
 * 包含：1，输入文件的名字；2，保存常量的类；3，输入文件；4，输出文件；5，断点文件；6，输入队列；7，输出队列等等。
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 * @param <IN> 输入队列保存的类型
 * @param <OUT> 输出队列保存的类型
 */
public class BpContext<IN, OUT> {

	/**
	 * 输入文件的名字。 <br>
	 * 一个输入文件对应一个{@code BpContext}对象。
	 */
	private String fileName;
	
	/**
	 * 保存常量的配置类。{@code BpProperties}
	 */
	private BpProperties properties;

	/**
	 * 输入文件。
	 */
	private File inFile;
	
	/**
	 * 输出文件。
	 */
	private File outFile;
	
	/**
	 * 保存断点的文件。
	 */
	private File bpFile;
	
	/**
	 * 解决“第一行”问题的文件。 <br>
	 * 备注：第一行问题：假设第一次运行，处理了2、3、4、5、6、8和9行，1行没有处理；那么记录断点的文件就记录2、3、4、5、6、8和9行； <br>
	 * 下次运行时，由于2到6行是连续的，所以从记录断点的文件中删除2到6行，删除之后为8和9行；这时，就直接跳过了没有处理的1行。
	 */
	private File rowOneFile;
	
	/**
	 * 输入队列。
	 */
	private LinkedBlockingQueue<BpObject<IN>> inQueue;
	
	/**
	 * 输出队列。
	 */
	private LinkedBlockingQueue<BpObject<OUT>> outQueue;
	
	/**
	 * 保存断点以及处理过的行号的{@code TreeSet}。
	 */
	private TreeSet<Integer> bpSet;
	
	/**
	 * 控制程序结束。 <br>
	 * true：生产者生产完毕，即读取输入文件结束，以及输入队列为空；否则false。
	 */
	private boolean exit;
	
	/**
	 * 必须有fileName和bpProperties。
	 * @param fileName 输入文件的名字
	 * @param bpProperties 保存常量的类
	 */
	public BpContext(String fileName, BpProperties bpProperties) {
		
		// 设置fileName。
		if (fileName == null || fileName.length() == 0) {
			new Exception("fileName is null or empty.").printStackTrace();
			System.exit(-1);	
		}
		this.fileName = fileName;

		// 设置bpProperties。
		if (bpProperties == null) {
			new Exception("BpProperties is null.").printStackTrace();
			System.exit(-1);	
		}
		this.properties = bpProperties;
		
		// 设置其他属性。
		initAttributes();
	}
	
	/**
	 * 配置除了fileName和bpProperties之外的其他属性。
	 */
	protected void initAttributes() {
		
		// 下面三行：得到输入目录、输出目录和临时目录。
		File inDir = new File(properties.getInDirPath());
		File outDir = new File(properties.getOutDirPath());
		File tmpDir = new File(properties.getTmpDirPath());
		
		// 下面三个if：判断对应目录是否存在，不存在则创建。
		if (!inDir.exists()) {
			if (!inDir.mkdir()) {
				new IOException("mkdir in directory failed.").printStackTrace();
				System.exit(-1);
			}
		}
		
		if (!outDir.exists()) {
			if (!outDir.mkdir()) {
				new IOException("mkdir out directory failed.").printStackTrace();
				System.exit(-1);
			}
		}
		
		if (!tmpDir.exists()) {
			if (!tmpDir.mkdir()) {
				new IOException("mkdir tmp directory failed.").printStackTrace();
				System.exit(-1);
			}
		}
		
		// 输入文件。
		setInFile(new File(inDir, fileName));
		
		// 输出文件：前缀加输入文件名。
		setOutFile(new File(outDir, properties.getOutFilePrefix() + fileName));
		
		// 记录断点的文件：前缀加输入文件名。
		setBpFile(new File(tmpDir, properties.getBpFilePrefix() + fileName));
		
		// 解决“第一行”问题的文件：前缀加输入文件名。“第一行”问题见该属性的备注。
		setRowOneFile(new File(tmpDir, properties.getRowOneFilePrefix() + fileName));
		
		// 输入队列，容量值在properties中。
		setInQueue(new LinkedBlockingQueue<BpObject<IN>>(properties.getInQueueCapacity()));

		// 输出队列，容量值在properties中。
		setOutQueue(new LinkedBlockingQueue<BpObject<OUT>>(properties.getOutQueueCapacity()));
		
		// 保存断点的TreeSet。
		setBpSet(new TreeSet<Integer>());
		
		// 程序结束设为false。
		setExit(false);
	}
	
	
	/*
	 * Getter, Setter and toString().
	 */
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public BpProperties getProperties() {
		return properties;
	}

	public void setProperties(BpProperties properties) {
		this.properties = properties;
	}

	public File getInFile() {
		return inFile;
	}

	public void setInFile(File inFile) {
		this.inFile = inFile;
	}

	public File getOutFile() {
		return outFile;
	}

	public void setOutFile(File outFile) {
		this.outFile = outFile;
	}

	public File getBpFile() {
		return bpFile;
	}

	public void setBpFile(File bpFile) {
		this.bpFile = bpFile;
	}

	public LinkedBlockingQueue<BpObject<IN>> getInQueue() {
		return inQueue;
	}

	public void setInQueue(LinkedBlockingQueue<BpObject<IN>> inQueue) {
		this.inQueue = inQueue;
	}

	public LinkedBlockingQueue<BpObject<OUT>> getOutQueue() {
		return outQueue;
	}

	public void setOutQueue(LinkedBlockingQueue<BpObject<OUT>> outQueue) {
		this.outQueue = outQueue;
	}

	public TreeSet<Integer> getBpSet() {
		return bpSet;
	}

	public void setBpSet(TreeSet<Integer> bpSet) {
		this.bpSet = bpSet;
	}

	public File getRowOneFile() {
		return rowOneFile;
	}

	public void setRowOneFile(File rowOneFile) {
		this.rowOneFile = rowOneFile;
	}

	public boolean isExit() {
		return exit;
	}

	public void setExit(boolean exit) {
		this.exit = exit;
	}

	@Override
	public String toString() {
		return "BpContext [fileName=" + fileName + ", properties=" + properties + ", inFile=" + inFile + ", outFile="
				+ outFile + ", bpFile=" + bpFile + ", rowOneFile=" + rowOneFile + "]";
	}
} 
