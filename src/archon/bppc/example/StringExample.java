package archon.bppc.example;

import archon.bppc.consumer.MidConsumer;
import archon.bppc.consumer.StringBpConsumer;
import archon.bppc.controller.BpController;
import archon.bppc.model.BpProperties;
import archon.bppc.pruducer.StringBpProducer;

/**
 * 例子类，简单演示整个过程。 <br>
 *  <br>
 * 可以参考该类来使用本项目。
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 */
public final class StringExample {
	
	// 准备好输入目录、输出目录以及临时目录。
	private static final String IN_DIR = "F:/bppc/in/";
	private static final String OUT_DIR = "F:/bppc/out/";
	private static final String TMP_DIR = "F:/bppc/tmp/";
	
	public static void main(String[] args) {
		
		// 设置常量配置类。
		BpProperties bpProperties = BpProperties.getDefaultBpProperties();
		bpProperties.setInDirPath(IN_DIR);
		bpProperties.setOutDirPath(OUT_DIR);
		bpProperties.setTmpDirPath(TMP_DIR);

		// 实现并实例化生产者、中间消费者以及写出消费者。
		StringBpProducer bpProducer = new StringBpProducer();
		StringBpConsumer bpConsumer = new StringBpConsumer();
		StringExampleMidConsumer midConsumer = new StringExampleMidConsumer();

		// 实例化主控类。
		BpController<String, String>  controller = new BpController<String, String>(bpProperties, bpProducer, midConsumer, bpConsumer);
		controller.run();
	}
}

/**
 * 例子类，继承并实现的中间消费者。
 * 
 * @author AlexGumHub 20160811
 *
 */
final class StringExampleMidConsumer extends MidConsumer<String, String>{

	@Override
	protected String transform(String inValue) {
		try {
			// 模拟中间消费者的处理过程。
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return inValue + "\ta";
	}
}
