package archon.bppc.example;

import archon.bppc.consumer.MidConsumer;
import archon.bppc.consumer.StringBpConsumer;
import archon.bppc.controller.BpController;
import archon.bppc.model.BpProperties;
import archon.bppc.pruducer.StringBpProducer;

/**
 * 例子类，简单演示整个过程，处理两个输入目录。
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 */
public final class StringExampleWithTwoInDirs {
	
	// 准备好输入目录、输出目录以及临时目录。
	private static final String IN_DIR = "F:/bppc/in/";
	private static final String OUT_DIR = "F:/bppc/out/";
	private static final String TMP_DIR = "F:/bppc/tmp/";
	
	// 输入目录2
	private static final String IN_DIR2 = "F:/bppc/in2/";
	private static final String OUT_DIR2 = "F:/bppc/out2/";
	private static final String TMP_DIR2 = "F:/bppc/tmp2/";
	
	public static void main(String[] args) {
		
		// 目录1
		BpProperties bpProperties = BpProperties.getDefaultBpProperties();
		bpProperties.setInDirPath(IN_DIR);
		bpProperties.setOutDirPath(OUT_DIR);
		bpProperties.setTmpDirPath(TMP_DIR);
		
		StringBpProducer bpProducer = new StringBpProducer();
		StringBpConsumer bpConsumer = new StringBpConsumer();
		StringExampleMidConsumer3 midConsumer = new StringExampleMidConsumer3();
		
		BpController<String, String>  controller = new BpController<String, String>(bpProperties, bpProducer, midConsumer, bpConsumer);
		controller.run();
		
		// 目录2
		BpProperties bpProperties2 = BpProperties.getDefaultBpProperties();
		bpProperties2.setInDirPath(IN_DIR2);
		bpProperties2.setOutDirPath(OUT_DIR2);
		bpProperties2.setTmpDirPath(TMP_DIR2);
		
		StringBpProducer bpProducer2 = new StringBpProducer();
		StringBpConsumer bpConsumer2 = new StringBpConsumer();
		StringExampleMidConsumer3 midConsumer2 = new StringExampleMidConsumer3();
		
		BpController<String, String>  controller2 = new BpController<String, String>(bpProperties2, bpProducer2, midConsumer2, bpConsumer2);
		controller2.run();
	}
}

/**
 * 例子类，继承并实现的中间消费者。
 * 
 * @author AlexGumHub 20160811
 *
 */
final class StringExampleMidConsumer3 extends MidConsumer<String, String>{

	@Override
	protected String transform(String inValue) {
		try {
			// 模拟中间消费者的处理过程。
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return inValue + "\ta";
	}
}

