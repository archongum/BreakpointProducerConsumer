package archon.bppc.example;

import archon.bppc.consumer.MidConsumer;
import archon.bppc.consumer.StringBpConsumer;
import archon.bppc.controller.BpController;
import archon.bppc.delegate.BpSimpleAdapter;
import archon.bppc.model.BpProperties;
import archon.bppc.pruducer.StringBpProducer;

/**
 * 例子类，简单演示整个过程，加入代理。
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 */
public final class StringExampleWithDelegate extends BpSimpleAdapter {
	
	// 准备好输入目录、输出目录以及临时目录。
	private static final String IN_DIR = "F:/bppc/in/";
	private static final String OUT_DIR = "F:/bppc/out/";
	private static final String TMP_DIR = "F:/bppc/tmp/";
	
	// 用在代理方法内，计数使用。
	static int bpProduceCount = 0;
	static int bpConsumeCount = 0;
	static int midConsumeCount = 0;
	
	public static void main(String[] args) {
		
		// 设置常量配置类。
		BpProperties bpProperties = BpProperties.getDefaultBpProperties();
		bpProperties.setInDirPath(IN_DIR);
		bpProperties.setOutDirPath(OUT_DIR);
		bpProperties.setTmpDirPath(TMP_DIR);

		// 实现并实例化生产者、中间消费者以及写出消费者。
		StringBpProducer bpProducer = new StringBpProducer();
		StringBpConsumer bpConsumer = new StringBpConsumer();
		StringExampleMidConsumer2 midConsumer = new StringExampleMidConsumer2();
		
		// 设置代理。
		StringExampleWithDelegate delegate = new StringExampleWithDelegate();
		bpProducer.setDelegate(delegate);
		bpConsumer.setDelegate(delegate);
		midConsumer.setDelegate(delegate);

		// 实例化主控类。
		BpController<String, String>  controller = new BpController<String, String>(bpProperties, bpProducer, midConsumer, bpConsumer);
		controller.run();
	}

	@Override
	public void midConsumedOnce() {
		System.out.println("midConsumedOnce" + ++midConsumeCount);
	}

	@Override
	public void midConsumptionFinished() {
		System.out.println("midConsumptionFinished");
	}

	@Override
	public void bpConsumedOnce() {
		System.out.println("bpConsumedOnce" + ++bpConsumeCount);
	}

	@Override
	public void bpConsumptionFinished() {
		System.out.println("bpConsumptionFinished");
	}

	@Override
	public void bpProducedOnce() {
		System.out.println("bpProducedOnce" + ++bpProduceCount);
	}

	@Override
	public void bpProducionFinished() {
		System.out.println("bpProducionFinished");
	}
}

/**
 * 例子类，继承并实现的中间消费者。
 * 
 * @author AlexGumHub 20160811
 *
 */
final class StringExampleMidConsumer2 extends MidConsumer<String, String>{

	@Override
	protected String transform(String inValue) {
		try {
			// 模拟中间消费者的处理过程。
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return inValue + "\ta";
	}
}
