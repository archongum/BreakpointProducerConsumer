package archon.bppc.pruducer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;

import archon.bppc.delegate.BpProducerDelegate;
import archon.bppc.model.BpContext;
import archon.bppc.model.BpObject;

/**
 * 生产者。 <br>
 * 负责读取输入文件，并把读取到的字符串数据转换成{@code BpObject<IN>}的{@code IN}类型，存放到输入队列中；转换过程重写方法： <br>
 * {@link #formatIN(String)}
 *  <br>
 * 使用说明：继承该抽象类，并重写方法：
 * {@link #formatIN(String)}
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 * @param <IN> 输入队列存放的数据类型{@code BpObject<IN>}的{@code IN}类型。
 */
public abstract class BpProducer<IN> implements Runnable {
	
	/**
	 * 上下文对象。 <br>
	 * {@code BpContext}
	 */
	private BpContext<IN, ?> context;
	
	/**
	 * 代理类。 <br>
	 * 选择使用。 <br>
	 * {@code BpProducerDelegate}
	 */
	private BpProducerDelegate delegate;
	
	/**
	 * {@code Runnable}
	 */
	public void run() {
		logic();
	}
	
	/**
	 * 主要逻辑方法。 <br>
	 * 在{@code Runnable}的{@link #run()}方法内执行。
	 */
	protected void logic() {
		
		// 跳过的行数。
		int skipRow = 0;
		
		// 判断断点文件是否存在。
		if (isBpFileExist()) {

			// 断点文件存在，把文件中的行号全部加载到bpSet中。
			setupBpSet();

			// 判断bpSet的第一个元素是否等于一。
			// 是：创建“row one file”，表示“第一行”问题解决了；否则，问题没有解决，不创建文件。
			if (isFirstOfBpSetEqualsOne()) createRowOneFile();

			// 判断“row one file”是否存在。
			if (isRowOneFileExist()) {
				
				// 整理bpSet，去掉前面连续的行号。
				arrangeBpSet();
				
				// 更新bpFile，即记录断点的文件。
				updateBpFile();
				
				// 设置跳过的行号数。
				skipRow = getSkipRow();
			}
		}

		// 读取输入文件，并把读取到的字符串处理之后，保存到输入队列中。
		setupInQueue(skipRow);
		
		// 读取完输入文件，通知代理。
		// 选择使用。
		if (delegate != null) delegate.bpProducionFinished();
		
		// 生产结束之后，设置程序结束标识符为true。
		setExit();
		
		// 生产者结局，把保存断点文件里面数据的bpSet设为null。
		// 可以不用。
		this.context.setBpSet(null);
	}
	
	/**
	 * 判断断点文件是否存在，即tmp目录下的"bp_"加输入文件名。
	 * @return exists()
	 */
	protected boolean isBpFileExist() {
		return context.getBpFile().exists();
	}
	
	/**
	 * 把记录断点文件里面的数据全部加载到bpSet中。 <br>
	 * {@code TreeSet<Integer>}
	 */
	protected void setupBpSet() {
		
		// 使用TreeSet。
		TreeSet<Integer> bpSet = context.getBpSet();

		// 一般输入流的操作。
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(context.getBpFile()), context.getProperties().getCharsetName()));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if (br == null) {
			new IOException("The BufferedReader of bpSet is null. Please start again.").printStackTrace();
			System.exit(-1);
		}

		String line = null;
		try {
			while ((line = br.readLine()) != null) {
				
				// 去除空行。
				if (line.length() == 0) {
					continue;
				}
				
				// 把文件中的数字转化为字符串并写入。
				bpSet.add(Integer.parseInt(line));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	/**
	 * 判断bpSet的第一个元素是否等于一。 <br>
	 * {@code TreeSet<Integer>}
	 * @return context.getBpSet().first() == 1
	 */
	protected boolean isFirstOfBpSetEqualsOne() {
		TreeSet<Integer> bpSet = context.getBpSet();
		if (bpSet.isEmpty()) return false;
		return bpSet.first() == 1;
	}
	
	/**
	 * 判断“记录‘第一行’问题是否解决”的文件是否存在，即tmp目录下的"ro_"加输入文件名。
	 * 
	 * @return exists()
	 */
	protected boolean isRowOneFileExist() {
		return context.getRowOneFile().exists();
	}
	
	/**
	 * 创建记录“第一行”问题是否解决的文件。
	 */
	protected void createRowOneFile() {
		try {
			context.getRowOneFile().createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 整理bpSet。 <br>
	 * {@code TreeSet<Integer>} <br>
	 * 实现功能：把开头连续的行号去除，但保留这些连续行号的最后那行； <br>
	 * 保留最后一行是为了，下次能知道是在哪里断开的（断点）。 <br>
	 * 备注：这个算法可以多样，能实现上述功能即可。
	 */
	protected void arrangeBpSet() {
		TreeSet<Integer> bpSet = context.getBpSet();
		Iterator<Integer> iterator = bpSet.iterator();

		int cursor = 0;
		int current = 0;
		while (iterator.hasNext()) {
			cursor = iterator.next();

			if (current == 0) {
				current = cursor;
				iterator.remove();
				continue;
			}
			
			if (cursor == current + 1) {
				iterator.remove();
				current = cursor;
			} else {
				break;
			}
		}
		bpSet.add(current);
	}

	/**
	 * 更新记录断点的文件。 <br>
	 * 把整理好的bpSet里面的所有内容，写入文件中。 <br>
	 * 说明：跟着该类的protected方法{@link #arrangeBpSet()}之后。
	 */
	protected void updateBpFile() {
		Iterator<Integer> iterator = context.getBpSet().iterator();
		
		// 一般输出流操作。
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(context.getBpFile()), context.getProperties().getCharsetName()));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if (bw == null) {
			System.err.println("The BufferedWriter of bpFile is null.");
		}
		
		// 遍历bpSet。
		while (iterator.hasNext()) {
			try {
				bw.write(iterator.next() + "\n");
				bw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获得需要跳过的行号数。 <br>
	 * 说明：跟着该类的protected方法{@link #arrangeBpSet()}之后。
	 * 
	 * @return 需要跳过的行号数。
	 */
	protected int getSkipRow() {
		TreeSet<Integer> bpSet = context.getBpSet();
		int i = bpSet.first();
		bpSet.remove(i);
		return i;
	}
	
	/**
	 * 把输入文件读取到输入队列中。
	 * 
	 * @param skipRow 需要跳过的行号数。
	 */
	protected void setupInQueue(int skipRow) {
		TreeSet<Integer> bpSet = context.getBpSet();
		LinkedBlockingQueue<BpObject<IN>> inQueue = context.getInQueue();
		
		// 一般输入流操作。
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(context.getInFile()), context.getProperties().getCharsetName()));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if (br == null) {
			new IOException("The BufferedReader of inFile is null.").printStackTrace();
			System.exit(-1);
		}

		String line = null;
		
		// 记录读取到的行号。
		int count = 0;
		try {
			while ((line = br.readLine()) != null) {
				
				// 去除空行。
				if (line.length() == 0) {
					continue;
				}

				// 加一行，空行不计算在内。
				count++;
				
				// 跳过skipRow
				if (count <= skipRow) {
					continue;
				}

				// 跳过bpSet里面已经处理过的行。
				if (!bpSet.isEmpty() && bpSet.contains(count)) {
					bpSet.remove(count);
					continue;
				}
				
				// 转换后存入。
				inQueue.put(new BpObject<IN>(count, formatIN(line)));
				
				// 每读取一行，通知代理。
				// 选择使用。
				if (delegate != null) delegate.bpProducedOnce();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 生产结束之后，设置程序结束标识符为true。
	 */
	protected void setExit() {
		LinkedBlockingQueue<BpObject<IN>> inQueue = context.getInQueue();
		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (inQueue.isEmpty()) {
				context.setExit(true);
				break;
			}
		}	
	}
	
	/**
	 * 字符串转换成{@code BpObject<IN>}的{@code IN}类型。
	 * 
	 * @param string 读取输入文件的一行字符串。
	 * @return {@code BpObject<IN>}的{@code IN}。
	 */
	protected abstract IN formatIN(String string);
	
	/**
	 * 设置代理。 <br>
	 * 选择使用。
	 * @param delegate 代理
	 */
	public void setDelegate(BpProducerDelegate delegate) {
		if (delegate != null) {
			this.delegate = delegate;
		}
	}
	
	
	/*
	 * Getter, Setter and toString().
	 */
	public BpContext<IN, ?> getContext() {
		return context;
	}

	public void setContext(BpContext<IN, ?> context) {
		this.context = context;
	}
}
