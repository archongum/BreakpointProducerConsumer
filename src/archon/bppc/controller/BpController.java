package archon.bppc.controller;

import java.io.File;

import archon.bppc.consumer.BpConsumer;
import archon.bppc.consumer.MidConsumer;
import archon.bppc.model.BpContext;
import archon.bppc.model.BpProperties;
import archon.bppc.pruducer.BpProducer;

/**
 * 主控类。 <br>
 * final，继承无意义，可以参考实现。 <br>
 * <br>
 * 本项目的设计模式是：生产者消费者模式。 <br>
 * 处理过程：输入文件{@literal ->}生产者{@literal ->}输入队列{@literal ->}中间消费者{@literal ->}输出队列{@literal ->}写出消费者。 <br>
 * 对应的类：{@code BpProducer<IN>}{@literal ->}{@code MidConsumer<IN, OUT>}{@literal ->}{@code BpConsumer<OUT>} <br>
 * <br> 
 * 使用说明：继承并实现上述三个抽象类，以及常量配置类，配置详情见{@code BpProperties}； <br>
 * 有了上述四个类之后，用着四个类作为参数创建此类{@code BpController<IN, OUT>}； <br>
 * 然后调用方法{@link #run()}即可。
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 * @param <IN> 输入队列存放的数据类型{@code BpObject<IN>}的{@code IN}类型。
 * @param <OUT> 输出队列存放的数据类型{@code BpObject<OUT>}的{@code OUT}类型
 */
public final class BpController<IN, OUT> implements Runnable {
	
	/**
	 * 上下文对象。 <br>
	 * {@code BpContext}
	 */
	private BpContext<IN, OUT> context;
	
	/**
	 * 常量配置类。
	 */
	private BpProperties bpProperties;
	
	/**
	 * 生产者。
	 */
	private BpProducer<IN> bpProducer;
	
	/**
	 * 中间消费者。
	 */
	private MidConsumer<IN, OUT> midConsumer;
	
	/**
	 * 写出消费者。
	 */
	private BpConsumer<OUT> bpConsumer;
	
	/**
	 * 
	 * @param bpProperties 常量配置类。
	 * @param bpProducer 生产者。
	 * @param midConsumer 中间消费者。
	 * @param bpConsumer 写出消费者。
	 */
	public BpController(BpProperties bpProperties, BpProducer<IN> bpProducer,
			MidConsumer<IN, OUT> midConsumer, BpConsumer<OUT> bpConsumer) {
		this.bpProperties = bpProperties;
		this.bpProducer = bpProducer;
		this.midConsumer = midConsumer;
		this.bpConsumer = bpConsumer;
	}
	
	/**
	 * {@code Runnable}
	 */
	public void run() {
		logic();
	}

	/**
	 * 主要逻辑方法。
	 * 在{@code Runnable}的{@link #run()}方法内执行。
	 */
	public void logic() {
		
		// 读取输入目录下的所有文件，不包含子目录。
		File inDir = new File(bpProperties.getInDirPath());
		for (File inFile : inDir.listFiles()) {
			
			// 跳过子目录。
			if (inFile.isDirectory()) continue;
			
			// 设置上下文。
			context = new BpContext<IN, OUT>(inFile.getName(), bpProperties);
			bpProducer.setContext(context);
			midConsumer.setContext(context);
			bpConsumer.setContext(context);
			
			// 开始处理一个文件。
			handleAFile();
		}
	} 
	
	/**
	 * 处理一个文件。
	 */
	private void handleAFile() {

		// 启动生产者线程。
		new Thread(bpProducer).start();

		// 启动中间消费者线程。
		int threadCount = context.getProperties().getMidConsumerThreadCount();
		for (int i = 0; i < threadCount; i++) {
			new Thread(midConsumer).start();
		}	

		// 启动写出消费者，在当前线程。
		bpConsumer.run();
	}

	
	/*
	 * Getter, Setter and toString().
	 */
	public BpContext<IN, OUT> getContext() {
		return context;
	}

	public void setContext(BpContext<IN, OUT> context) {
		this.context = context;
	}

	public BpProperties getBpProperties() {
		return bpProperties;
	}

	public void setBpProperties(BpProperties bpProperties) {
		this.bpProperties = bpProperties;
	}

	public BpProducer<IN> getBpProducer() {
		return bpProducer;
	}

	public void setBpProducer(BpProducer<IN> bpProducer) {
		this.bpProducer = bpProducer;
	}

	public MidConsumer<IN, OUT> getMidConsumer() {
		return midConsumer;
	}

	public void setMidConsumer(MidConsumer<IN, OUT> midConsumer) {
		this.midConsumer = midConsumer;
	}

	public BpConsumer<OUT> getBpConsumer() {
		return bpConsumer;
	}

	public void setBpConsumer(BpConsumer<OUT> bpConsumer) {
		this.bpConsumer = bpConsumer;
	}
}
