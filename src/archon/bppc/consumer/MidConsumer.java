package archon.bppc.consumer;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import archon.bppc.delegate.MidConsumerDelegate;
import archon.bppc.model.BpContext;
import archon.bppc.model.BpObject;

/**
 * 输入队列和输出队列之间的消费者。 <br>
 * 读取输入队列的数据，处理之后，放到输出队列。 <br>
 * 处理过程：
 * {@link #transform(Object)}
 *  <br>
 * 使用说明：继承该抽象类，并重写方法：
 * {@link #transform(Object)}
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 * @param <IN> 输入队列存放的数据类型{@code BpObject<IN>}的{@code IN}类型。
 * @param <OUT> 输出队列存放的数据类型{@code BpObject<OUT>}的{@code OUT}类型。
 */
public abstract class MidConsumer<IN, OUT> implements Runnable {
	
	/**
	 * 上下文对象。 <br>
	 * {@code BpContext}
	 */
	private BpContext<IN, OUT> context;

	/**
	 * 代理类。 <br>
	 * 选择使用。 <br>
	 * {@code BpProducerDelegate}
	 */
	private MidConsumerDelegate delegate;

	/**
	 * {@code Runnable}
	 */
	public void run() {
		logic();
	}
	
	/**
	 * 主要逻辑方法。 <br>
	 * 在{@code Runnable}的{@link #run()}方法内执行。
	 */
	protected void logic() {
		
		// 程序还没结束，即生产者还在生产以及输入队列不为空。
		while (!context.isExit()) {
			pollAndPut();
		} 
		
		// 该类的消费者结束，通知代理。
		// 选择使用。
		if (delegate != null) delegate.midConsumptionFinished();
	}
	
	/**
	 * 读取输入队列的数据，处理，放到输出队列。
	 */
	protected void pollAndPut() {
		LinkedBlockingQueue<BpObject<IN>> inQueue = context.getInQueue();
		LinkedBlockingQueue<BpObject<OUT>> outQueue = context.getOutQueue();
		
		// 读取输入队列的超时。
		int timeout = context.getProperties().getMidConsumerPollTimeoutMs();

		BpObject<IN> bpIn = null;
		try {
			while ((bpIn = inQueue.poll(timeout, TimeUnit.MILLISECONDS)) != null) {
				
				// 处理。
				handle(bpIn, outQueue);
				
				// 每处理完一行，通知代理。
				// 选择使用。
				if (delegate != null) delegate.midConsumedOnce();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 从输入队列取得数据之后的处理过程。<br>
	 * 默认把结果存到输出队列。<br>
	 * 由于该方法内调用了方法{@link #transform(Object)}；<br>
	 * 所有如果重写了该方法而且重写的方法内没有调用super的该方法，那么方法{@link #transform(Object)}就没作用了，即可以任意实现，不影响。
	 * 
	 * @param bpIn 从输入队列取得的数据{@code BpObject}
	 */
	protected void handle(BpObject<IN> bpIn, LinkedBlockingQueue<BpObject<OUT>> outQueue) {
		try {
			outQueue.put(new BpObject<OUT>(bpIn.getRow(), transform(bpIn.getValue())));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * {@code BpObject<IN>}的{@code IN}转换{@code BpObject<OUT>}的{@code OUT}
	 * 
	 * @param inValue {@code BpObject<IN>}的{@code IN}
	 * @return {@code BpObject<OUT>}的{@code OUT}。
	 */
	protected abstract OUT transform(IN inValue);
	
	/**
	 * 设置代理。 <br>
	 * 选择使用。
	 * @param delegate 代理
	 */
	public void setDelegate(MidConsumerDelegate delegate) {
		if (delegate != null) {
			this.delegate = delegate;
		}
	}	

	
	/*
	 * Getter, Setter and toString().
	 */
	public BpContext<IN, OUT> getContext() {
		return context;
	}

	public void setContext(BpContext<IN, OUT> context) {
		this.context = context;
	}
}
