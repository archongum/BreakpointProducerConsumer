package archon.bppc.consumer;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import archon.bppc.delegate.BpConsumerDelegate;
import archon.bppc.model.BpContext;
import archon.bppc.model.BpObject;

/**
 * 把输出队列写出输出文件的消费者。 <br>
 * 负责读取输出队列，并把读取到的数据类型{@code BpObject<OUT>}的{@code OUT}类型转换成字符串后，写出到输出文件中；转换过程重写方法：
 * {@link #formatOUT(Object)}
 *  <br>
 * 使用说明：继承该抽象类，并重写方法： 
 * {@link #formatOUT(Object)}
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 * @param <OUT> 输出队列存放的数据类型{@code BpObject<OUT>}的{@code OUT}类型。
 */
public abstract class BpConsumer<OUT> implements Runnable {
	
	/**
	 * 上下文对象。 <br>
	 * {@code BpContext}
	 */
	private BpContext<?, OUT> context;
	
	/**
	 * 代理类。 <br>
	 * 选择使用。 <br>
	 * {@code BpProducerDelegate}
	 */
	private BpConsumerDelegate delegate;
	
	/**
	 * {@code Runnable}
	 */
	public void run() {
		logic();
	}

	/**
	 * 主要逻辑方法。 <br>
	 * 在{@code Runnable}的{@link #run()}方法内执行。
	 */
	protected void logic() {
		
		// 程序还没结束，即生产者还在生产以及输入队列不为空。
		while (!context.isExit()) {
			writeOut();
		}
		
		// 该类的消费者结束，通知代理。
		// 选择使用。
		if (delegate != null) delegate.bpConsumptionFinished();
	}
	
	/**
	 * 把输出队列的数据写出输出文件中。
	 */
	protected void writeOut() {
		LinkedBlockingQueue<BpObject<OUT>> outQueue = context.getOutQueue();
		String charset = context.getProperties().getCharsetName();
		
		// 读取输出队列的超时。
		int timeout = context.getProperties().getBpConsumerPollTimeoutMs();

		BufferedWriter outFileBw = null;
		BufferedWriter bpFileBw = null;
		try {
			outFileBw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(context.getOutFile(), true), charset));
			bpFileBw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(context.getBpFile(), true), charset));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if (outFileBw == null || bpFileBw == null) {
			new IOException("The BufferedWriter of outFile or bpFile is null. Please try again.").printStackTrace();
			System.exit(-1);
		}
		
		BpObject<OUT> bpOut = null;
		try {
			while ((bpOut = outQueue.poll(timeout, TimeUnit.MILLISECONDS)) != null) {
				try {
					// 转换后写出。
					handle(bpOut, outFileBw);
					
					// 写出行号
					bpFileBw.write(bpOut.getRow() + "\n");
					bpFileBw.flush();
					
					// 每写出一行，通知代理。
					// 选择使用。
					if (delegate != null) delegate.bpConsumedOnce();
				} catch (IOException e) {
					e.printStackTrace();
				} 
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			try {
				outFileBw.close();
				bpFileBw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 从输出队列取得数据之后的后续处理。<br>
	 * 默认把结果存到默认的输出文件。<br>
	 * 由于该方法内调用了方法{@link #formatOUT(Object)}；<br>
	 * 所有如果重写了该方法而且重写的方法内没有调用super的该方法，那么方法{@link #formatOUT(Object)}就没作用了，即可以任意实现，不影响。
	 * 
	 * @param bpOut	从输出列取得的数据{@code BpObject}
	 * @param outFileBw 默认输出文件的{@code BufferedWriter}，不能调用close()方法！
	 */
	protected void handle(BpObject<OUT> bpOut, BufferedWriter outFileBw) {
		try {
			outFileBw.write(formatOUT(bpOut.getValue()) + "\n");
			outFileBw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * {@code BpObject<OUT>}的{@code OUT}类型转换字符串。
	 * 
	 * @param out {@code BpObject<OUT>}的{@code OUT}。
	 * @return 写出到输出文件的字符串。
	 */
	protected abstract String formatOUT(OUT out);

	/**
	 * 设置代理。 <br>
	 * 选择使用。
	 * @param delegate 代理
	 */
	public void setDelegate(BpConsumerDelegate delegate) {
		if (delegate != null) {
			this.delegate = delegate;
		}
	}


	/*
	 * Getter, Setter and toString().
	 */
	public BpContext<?, OUT> getContext() {
		return context;
	}

	public void setContext(BpContext<?, OUT> context) {
		this.context = context;
	}
}
