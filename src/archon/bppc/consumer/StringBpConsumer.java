package archon.bppc.consumer;

/**
 * 继承{@code BpConsumer<T>}，并指定{@code T}为{@code String}类型。 <br>
 *  <br>
 * 说明：一般的需求通常是，把输入文件的每一行以字符串类型存放到输入队列中； <br>
 * 所以，这种情况下，可以用此类{@code StringBpProducer}作为生产者{@code BpConsumer<T>}。 <br>
 *  <br>
 * 适用需求：打算在输出队列里面存放{@code String}类型的数据。
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 */
public class StringBpConsumer extends BpConsumer<String> {

	@Override
	protected String formatOUT(String out) {
		return out;
	}
}
