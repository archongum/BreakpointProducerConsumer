package archon.bppc.delegate;

/**
 * {@code MidConsumer<IN, OUT>}的代理。
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 */
public interface MidConsumerDelegate {
	
	/**
	 * 每处理一行。
	 */
	void midConsumedOnce();
	
	/**
	 * 全部处理完毕。
	 */
	void midConsumptionFinished();
}
