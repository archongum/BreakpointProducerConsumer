package archon.bppc.delegate;

/**
 * {@code BpConsumer<OUT>}的代理。
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 */
public interface BpConsumerDelegate {
	
	/**
	 * 每写出一行。
	 */
	void bpConsumedOnce();
	
	/**
	 * 全部写出完毕。
	 */
	void bpConsumptionFinished();
}