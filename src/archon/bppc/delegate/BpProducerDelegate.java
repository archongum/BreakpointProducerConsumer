package archon.bppc.delegate;

/**
 * {@code BpProducer<IN>}的代理。
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 */
public interface BpProducerDelegate {
	
	/**
	 * 读取一行。
	 */
	void bpProducedOnce();
	
	/**
	 * 全部读取完毕。
	 */
	void bpProducionFinished();
}
