package archon.bppc.delegate;

/**
 * 全部代理的空实现。 <br>
 *  <br>
 * {@code BpProducerDelegate, BpConsumerDelegate, MidConsumerDelegate}
 * 
 * @author AlexGumHub 20160811
 * @since 1.7
 *
 */
public abstract class BpSimpleAdapter implements BpProducerDelegate, BpConsumerDelegate, MidConsumerDelegate {

	public void midConsumedOnce() {}

	public void midConsumptionFinished() {}

	public void bpConsumedOnce() {}

	public void bpConsumptionFinished() {}

	public void bpProducedOnce() {}

	public void bpProducionFinished() {}
}
