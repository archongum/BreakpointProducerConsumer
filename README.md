# **[BpPC（Breakpoint Producer-Consumer）][1]**

## 项目介绍：

> 实现文件读取的断点记录，下次在断点位置继续读取。
> 解决读取文件时突然中断的问题，如断电导致电脑关机。

## 设计模式：
![设计模式](http://git.oschina.net/uploads/images/2016/0814/173204_77a7f63e_557717.png "设计模式")
<!--```flow-->
<!--op1=>operation: 输入文件:File-->
<!--op2=>operation: 生产者:BpProducer-->
<!--op3=>operation: 输入队列:Queue-->
<!--op4=>operation: 中间消费者:MidConsumer-->
<!--op5=>operation: 输出队列:Queue-->
<!--op6=>operation: 写出消费者:BpConsumer-->
<!--op7=>operation: 输出文件:File-->

<!--op1(right)->op2(right)->op3(right)->op4->op5->op6->op7-->

<!--```-->
	
## 项目组成：
```java
archon.bppc.model.
	BpContext: 上下文对象，保存输入输出文件、队列以及常量配置类BpProperties等；
	BpObject: 输入输出队列的通用数据类型；
	BpProperties: 常量配置类。
	
archon.bppc.pruducer.
	abstract BpProducer: 生产者；
	StringBpProducer: String类型的生产者。
	
archon.bppc.consumer.
	abstract BpConsumer: 写出消费者;
	abstract MidConsumer: 中间消费者；
	StringBpConsumer: String类型的写出消费者。
	
archon.bppc.controller.
	final BpController: 主控类，控制生产到消费的全过程。

archon.bppc.delegate: 代理接口。

archon.bppc.example: 演示程序。
```
## 功能：

> 串行处理多目录下的多文件，但不包含子目录。每个目录都需要单独配置。（可以实现并行处理）
	
## 简单使用：
> 引入包。（提供了Ant打包，结果在build文件夹下。）

```java
	public final class StringExample {

		// 准备好输入目录、输出目录以及临时目录。
		private static final String IN_DIR = "F:/bppc/in/";
		private static final String OUT_DIR = "F:/bppc/out/";
		private static final String TMP_DIR = "F:/bppc/tmp/";
		
		public static void main(String[] args) {
			
			// 设置常量配置类。
			BpProperties bpProperties = BpProperties.getDefaultBpProperties();
			bpProperties.setInDirPath(IN_DIR);
			bpProperties.setOutDirPath(OUT_DIR);
			bpProperties.setTmpDirPath(TMP_DIR);
	
			// 实现并实例化生产者、中间消费者以及写出消费者。
			BpProducer bpProducer = new StringBpProducer();
			BpConsumer bpConsumer = new StringBpConsumer();
			MidConsumer midConsumer = new MidConsumer() {
				@Override
				protected String transform(String inValue) {
					try {
						// 模拟中间消费者的处理过程。
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					return inValue + "\ta";
				}
				
			}
	
			// 实例化主控类。
			BpController<String, String>  controller = new BpController<String, String>(bpProperties, bpProducer, midConsumer, bpConsumer);
			controller.run();
		}
	}
```
更多用法在example包里。
## 资源
### Github: [AlexGumHub / BreakpointProducerConsumer][1]
### OSChina: [AlexGum / BreakpointProducerConsumer][2]
[1]: https://github.com/AlexGumHub/BreakpointProducerConsumer.git
[2]: http://git.oschina.net/alexgum/BreakpointProducerConsumer
